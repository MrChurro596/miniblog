$(function () {
    var APPLICATION_ID = "ABA7AA5F-EC00-0A92-FFAD-5802899AB200",
        SECRET_KEY = "490A6A50-2108-C228-FF96-406D89A46000",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
     
    var postsCollection = Backendless.Persistence.of(Posts).find();   
     
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY");
    });
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.maincontainer').html(blogHTML);
    
    var blogScriptB = $("#title-template").html();
    var blogTemplateB = Handlebars.compile(blogScriptB);
    var blogHTMLB = blogTemplateB(wrapper);
    
    $('#placeholder').html(blogHTMLB);
    
    $(".button-collapse").sideNav();
     
});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}
